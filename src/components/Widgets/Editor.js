import React from 'react'
import { Button, Form } from 'react-bootstrap'

export default class Editor extends React.Component{
    constructor(props){
        super(props)
        this.state = {

        }
    }
    componentDidMount(){
        Object.keys(this.props.data).map((key,j)=>{
          
            this.setState({
                [key]:this.props.data[key]
            })
        return 1   
        })
    }
    
    onChange(e){
        this.props.onChange(e.target.name,e.target.value)
    
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    populate(){

        return Object.keys(this.props.data).map((key,j)=>{

             if(this.props[key]===undefined){
                 return <Form>
                         <Form.Text htmlFor={key}>{key.toLocaleUpperCase()}</Form.Text>   
                         <input onChange={(e)=>this.onChange(e)} name = {key} value={this.state[key]}/>
                     </Form>
             }
             else{
                 if(this.props[key].render===undefined || this.props[key].render===true){
                     if(this.props[key].custom===undefined){
                        return <Form>
                        <Form.Text htmlFor={key}>{key.toLocaleUpperCase()}</Form.Text>   
                        <input onChange={(e)=>this.onChange(e)} name = {key} value={this.state[key]}/>
                        </Form>
                     }
                     else{
                         return (
                             <Form >
                             <Form.Text htmlFor={key}>{key.toLocaleUpperCase()}</Form.Text>
                             {this.props[key].custom}
                         </Form>
                        )
                     }
                 }
             }
         }
        )
    }

    render(){
        return(
            <div>
                <div>

                </div>

                <div>
                    <form onSubmit={this.props.callback} >
                        {this.populate()}
        {this.props.callback&&<Button style={{marginTop:'5px'}}>Save</Button>}    
                    </form>    
                </div>    
            </div>
        )
    }
} 


