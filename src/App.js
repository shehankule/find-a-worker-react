import React from 'react';
import { BrowserRouter as Router, Route, Redirect,Switch } from "react-router-dom";
import Home from './views/home'
import DashView from './views/dash';
import TopNav from './components/Widgets/Navigation/topnav'
import UserLogin from './components/User/Login';
import SignUp from './components/User/signup';
import NotFound from './views/Error/404'

function isAuth(){
  console.log(localStorage.getItem('sessionEmail'))
  if(localStorage.getItem('sessionEmail')!==null){
    return true
  }
  else{
    return true}
}

function App() {
  return (

    <Router>
    <TopNav style={{position:'absolute'}}/>
      
        <Route path='/' exact component={Home} />
        <Route path='/login' exact component={UserLogin}/>
        <Route path='/signup' exact component={SignUp}/>
        <Route path='/client'  render={props=>isAuth()?<DashView {...props} />:<UserLogin {...props}/>} />
        <Route path='/worker'  render={props=>isAuth()?<DashView {...props} />:<UserLogin {...props}/>} />
        <Route path='/404' component={NotFound} />
    </Router>

  )


}

export default App;
