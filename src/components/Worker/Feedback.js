import React from 'react'
import {Form,Button} from 'react-bootstrap'
export default function(){
    if(localStorage.getItem('sessionType')==='worker'){
        return null
    }
    return(
        <Form>
            <h5>Rate this Job</h5>
            <Form.Control style={{resize:'none'}} name="Decription" as="textarea" rows="3" maxLength='250' maxRows='4'/>
            <Button>Submit</Button>
        </Form>
    )
}