import React from 'react'
import {Accordion,Form,Button,Row,Col} from 'react-bootstrap'
import DropDown from '../Widgets/DropDown';

export default class Searchbar extends React.Component{
    constructor(props){
        super(props)
        this.state={
            SkillId:null,
            Description:''
        }
        
    }
    
    componentDidMount(){
        if(localStorage.getItem('sessionType')!=='worker'){
            this.setState({
                client:true
            })
        }
    }
    onChange(e){
        if(e.target!==undefined && this.props.callback !==undefined){
            this.props.callback('Description',e.target.value)
        }
        else{
            console.log(e.value)
            this.props.callback('SkillId',e.value)
        }
    }


  
    render(){
        // if(!this.state.client){return null}
        return(
            <Row style={{margin:'0'}}>
            <Col style={{display:this.props.sides?this.props.sides:"flex"}} md={2} sm={12}></Col>
            <Col md={this.props.top?12:8} sm={12} xs={12} style={{marginTop:this.props.top?this.props.top:'400px',marginBottom:this.props.top?'10px':'50px',backgroundColor:'rgba(100, 100, 100, 0.2)',paddingTop:'10px'}}>
            <Form>
                <Form.Group  controlId="formBasic">
                    <DropDown callback ={this.onChange.bind(this)} name="SkillId" data={this.props.dropdown} title="Select Work Category" />
                    {/* <Form.Text> Describe Your Work</Form.Text> */}
                    {/* <Form.Control style={{resize: 'none'}} onChange={this.onChange.bind(this)} name="Decription" as="textarea" rows="2" maxLength='250' maxRows='3'/> */}
                    {this.state.client&&localStorage.getItem('sessionID')!==null?<Button onClick={this.props.bookNow} style={{float:'',marginTop:'10px',marginBottom:'5px',marginRight:'5px'}} >Book Now</Button>:null}
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                    {/* {this.state.client?<Button onClick={this.props.later} style={{float:'right',marginTop:'10px', marginBottom:'5px'}}>Book Later</Button>:null} */}
                    </Accordion.Toggle>
                </Form.Group>
            </Form>
            </Col >
            <Col style={{display:this.props.sides?this.props.sides:"flex"}} md={2}></Col>
            </Row>
        )
    }

}