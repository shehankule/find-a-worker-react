import React from 'react'
import {Spinner} from 'react-bootstrap'
export default class GetSkillName extends React.Component{
    constructor(props){
        super(props)
        this.state={
            name:null
        }
    }

    componentDidMount(){
        fetch('http://localhost:3000/dataservices/getallskills',{
            method:'GET',
            credentials:'include',
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res.recordset)
            res.recordset.forEach(skill=>{
               
                if(skill.SkillId===this.props.id){
                    console.log(skill.SkillId)
                    this.setState({
                        name: skill.SkillTitle})
                    }
                })
        })
    }

    render(){
        if(!this.state.name){return <span><Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
    </Spinner> <small>Please Wait</small></span>}
        return(
            <span>{this.state.name.charAt(0).toUpperCase()+ this.state.name.slice(1)}</span>
        )
    }
}