import React from 'react'
import{Card,Col,Row,Button} from 'react-bootstrap';
import OrderApplet from './OrderApplet'
import DateTimeViewer from '../Widgets/DateTimeViewer'
import WorkerController from '../Worker/RequestController'
import MapViewer from '../Widgets/MapViewer'
import GetLocation from '../../helpers/getLocation'
import OrderController from '../Worker/OrderController';
import Feedback from '../Worker/Feedback';

export default class OrderContainer extends React.Component{
    constructor(props){
        super(props)
        this.state={
            OrderLocationNew:null,
            SkillId:null,
            data :null,
            location:null
       
        }
    }
    
    componentDidMount(){

        fetch('http://localhost:3000/ordersWorker/getOrderDetails/'+this.props.match.params.id,{credentials:'include'})
        .then(res=>res.json())
        .then(res=>{

            if(res.result===undefined){
                this.setState({
                    data:null
                })
                document.location.replace('/404')
                return null}
            else{
                if(res.result[0]!==undefined){
                    if(res.result[0][0]!==undefined){
                        this.setState({
                            data:res.result[0][0],
                        },)
                        console.log(this.state.data)
                        GetLocation(res.result[0][0].OrderLoaction.split(','),(x,y)=>{
                            this.setState({
                                location:y
                            })
                        })
                    }
                }
       
            }    
        })

        
        
    }

    updateStatecb(key,value){
        console.log(value)
        this.setState({
            [key]:value
        })

    }


    render(){
 
        const data = this.state.data
        
        if(data!==null){ 
            return(
                <>
                    <Card >
    
                    <Card.Body>
    
                    <Card.Title>
                        {/* <p>{JSON.stringify(data)}</p>
                        <p>{JSON.stringify(this.state)}</p> */}
               
                        <h3><span>Job Near</span> {this.state.location/*Geocode the Coordinates*/}</h3><br/> 

               
               </Card.Title>
    
                 <Card.Text>
                     <Row style={{height:'800px'}}>
                       <Col md={4}>
                   
                                <DateTimeViewer
                                    date = {data.OrderDate} 
                                    Start = {data.ExpectedStartTime }
                                    End = {data.ExpectedEndTime}
                                />

                                <p><b>Estimated Charge:</b><br/>
                                {data.ExpectedPrice?data.ExpectedPrice+'/=':'N/A'}</p>

                                <OrderController
                                    ClientId = {data.ClientId}
                                    Id={data.OrderId}
                                    isWorker = {true}
                                    isStarted = {data.StartTime}
                                    isEnded = {data.EndTime}
                                />
                                <Feedback/>
                            </Col>

                             <Col md={8} >
                               
                             <MapViewer
                                center = {{  lat: this.state.data.OrderLoaction.split(',')[0], lng: this.state.data.OrderLoaction.split(',')[1]}} /* To Be Passed From the Dataset*/ 
                             marker ={{ lat: data.OrderLoaction.split(',')[0], lng: data.OrderLoaction.split(',')[1]}} /* To Be Passed from the dataset*/
                                zoom={18}
                               style={{height:'70%',width:'70%',marginLeft:'0px',boxShadow: '0px 9px 14px 0px rgba(0,0,0,0.29)'}}
                             />
                             </Col>
    
                        </Row>   
    
                     </Card.Text>
                    </Card.Body>
    
                     </Card>
                    
    
                 </>
             
             )
        }
        else{
            return null
        }
    }
}