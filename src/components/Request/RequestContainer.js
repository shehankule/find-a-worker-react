import React from 'react';
import ReadMoreReact from 'read-more-react';
import{Card,Col,Row,Button} from 'react-bootstrap';
import RequestApplet from './RequestApplet'
import UserApplet from '../User/UserApplet'
import DateTimeViewer from '../Widgets/DateTimeViewer'
import WorkerController from '../Worker/RequestController'
import MapViewer from '../Widgets/MapViewer'
import GetLocation from '../../helpers/getLocation'

import Editor from '../Widgets/Editor'
import DropDown from '../Widgets/DropDown'
import LocationPicker from '../Widgets/LocationPicker'

export default class RequestContainer extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            OrderLocationNew:null,
            SkillId:null,
            test:'PICKER',
            data :null,
            location:null
        }
    }
    
    componentWillMount(){
        fetch('http://localhost:3000/requests/show/'+this.props.match.params.id,{credentials:'include'})
        .then(res=>res.json())
        .then(res=>{
            if(res.recordset===undefined || res.recordset[0]===undefined){
               
                this.setState({
                    data:null
                })
                document.location.replace('/404')
                return null}
            else{

                this.setState({
                    data:res.recordset[0]
                },)
                console.log(res)
            }    
        })

        
        
    }

    setOrderLocation(e,map,coords){
        const lat = coords.latLng.lat()
        const lng = coords.latLng.lng()
        this.setState({
            OrderLocationNew:{lat,lng} 
        },()=>(console.log(this.state.OrderLocationNew)))
       
    }

    setSkillId(){

    }

    updateStatecb(key,value){
        console.log(value)
        this.setState({
            [key]:value
        })
    }
 

    render(){
 
        const data = this.state.data
        console.log(data)
        if(data===null){return null}
        // return(<p>{data.Description}</p>)
        return(
            <>
                <Card >

                <Card.Body>

                <Card.Title>

                 <RequestApplet 
                        SkillId = {data.SkillId}
                        OrderLocation = {this.state.location===null?GetLocation(data.OrderLocation.split(','),this.updateStatecb.bind(this)):this.state.location}
                        // CreatedDate = {data.CreatedDate}
                        />
           </Card.Title>

             <Card.Text>
                 <Row style={{height:'800px'}}>
                   <Col md={4}>
                       <ReadMoreReact
                                text = {data.Description?data.Description:''}
                                readMoreText={<Button variant="link">Read More</Button>}
                            />
                            <DateTimeViewer
                                date = {data.OrderDate} 
                                Start = {data.StartTime}
                                End = {data.ExpectedEndTime}
                            />
                            <WorkerController
                                ClientId = {data.ClientId}
                                Id={data.RequestId}
                                isWorker = {true}
                            />

                        </Col>

                         <Col md={8} >
                           
                         <MapViewer
                            center = {{  lat: data.OrderLocation.split(',')[0], lng: data.OrderLocation.split(',')[1]}} 
                         marker ={{ lat: data.OrderLocation.split(',')[0], lng: data.OrderLocation.split(',')[1]}} 
                            zoom={18}
                           style={{height:'70%',width:'70%',marginLeft:'0px',boxShadow: '0px 9px 14px 0px rgba(0,0,0,0.29)'}}
                         />
                         </Col>

                    </Row>   

                 </Card.Text>
                </Card.Body>
                
                 {/* <Editor 
                     data = {data}
                    RequestId = {{render:false}}
                     ClientId = {{render:false}}
                    StartTime = {{type:'time'}}
                     EndTime = {{type:'time'}}
                     OrderDate = {{type:'date'}}
                 OrderLocation = {{custom: <LocationPicker 
                     center = {{ lat: 6.9128, lng: 79.8507}}/* To be Passed from dataset*/ 
                    /*marker={this.state.OrderLocationNew} 
                        callback = {this.setOrderLocation.bind(this)}/>}}
                    OrderLocationValue = {this.state.OrderLocationValue}
                    SkillId = {{custom:<DropDown/>}}
                    SkillIdValue = {this.state.SkillIdValue}

                     /> */}
                
               
              

            



                 </Card>
                

             </>
         
         )
    }
}

