import React from 'react'
import {Table,} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import getLocation from '../../helpers/getLocation'
import GetSkillName from '../../helpers/getSkillName';
import GetLocation ,{GetLocationName} from '../../helpers/getLocation';
import Loading from '../Widgets/Loading';

export default class MyRequests extends React.Component{
    constructor(props){
        super(props)
        this.state={
            data:null,
            location:null
        }
    }
    componentWillMount(){
        if(localStorage.getItem('sessionType')==='worker'){

            fetch('http://localhost:3000/requests/pool/worker/'+localStorage.getItem('sessionID'),{credentials:'include'})
            .then(res=>res.json())
            .then(res=>{
                console.log(res)
                if(res.result!==undefined){
                    this.setState({
                        data:res.result[0]
                    })
                }
                })
        }
        else{
                fetch('http://localhost:3000/requests/owner/'+localStorage.getItem('sessionID'),{credentials:'include'})
                .then(res=>res.json())
                .then(res=>{
                    
                    console.log(res)
                    if(res.result!==undefined){
                        this.setState({
                            data:res.result[0]
                        })
                    }else{
                        this.setState({
                            data: false
                        })
                    }
                })
            }
        }
    

    updateStatecb(key,value,id){
        console.log(this.state)
        this.setState({
            [id]:value
        })
    }
 

    render(){
        const data = this.state.data

        if(data===null){return <Loading/>}
        if(data===false){return <p>No Data Found</p>}
        return (
            <Table responsive hover>
                
                    <thead>
                        <th>#</th>
                        <th>Work Type</th>
                        <th>Near</th>
                        <th>On</th>
                        <th>Expected Start Time</th>
                        <th>Expected End Time</th>
                        <th></th>
                    </thead>
                    <tbody>
    
                          {data?data.map((request,key)=>{
                              let date = new Date(request.OrderDate),
                              start = new Date(request.StartTime),
                              end = new Date(request.ExpectedEndTime)
                                console.log(request)
                              return <tr>
                                  <td>{key+1}</td>
                                  <td><GetSkillName id={request.SkillId}/></td>
                                  <td><GetLocationName location={request.OrderLocation?request.OrderLocation.split(','):null}/></td>
                                  <td>{ date.toDateString() }</td>
                                  <td>{start.toUTCString('en-US').split(' ')[4]}</td>
                                  <td>{end.toUTCString('en-US').split(' ')[4]}</td>
                                  <td><Link to={"/client/view/"+request.RequestId}>View</Link></td>
                              </tr>
                          }):null}
                    </tbody>
                  
            </Table>
    

)
}}