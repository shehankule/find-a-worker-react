import React from 'react'

export default function(props){
    var Start = new Date(props.Start),
    End = new Date(props.End),
    date = new Date(props.date)
    return(
        <div className="">
            {props.date?
            <div>
            <p><b>Date</b><br/>
            {date.toDateString() /*Format Date*/}</p> 
            </div>
            :null}

            {props.Start && props.End?
            <div>
            <p><b>During</b><br/>
            {Start.toUTCString('en-US').split(' ')[4] + " - " + End.toUTCString('en-US').split(' ')[4] /*Format Time Stamps*/}</p> 
            </div>
            :null}

        </div>
    )
}