import React from 'react'
import { Col,Form, Row,Modal,Card, Image, Table, Button, ButtonGroup, ListGroup, Dropdown } from 'react-bootstrap';
import Loading from '../Widgets/Loading';
import StarRatings from 'react-star-ratings';
import {GetLocationName} from '../../helpers/getLocation'
import Switch from "react-switch";
import Editor from '../Widgets/Editor';
import DropDown from '../Widgets/DropDown';

export default class UserProfile extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            editable:false,
            locations:[],
            userEdit:[],
            skills:[]
        }
        this.User = null
    }


// Replace invokation on faker() with actual API call
    componentWillMount(){
        fetch('http://localhost:3000/dataservices/getalllocations',{credentials:'include'})
        .then(res=>res.json())
        .then(res=>{
            try{
                res.recordset.forEach(record => {
                    
                this.setState({
                    locations:this.state.locations.concat({value:record.DivisionalSecretary,label:record.DivisionalSecretary})
                })
            });
                console.log(this.state)
            }
            catch(err){
                console.log(err)
            }
        })
    
      
        if(localStorage.getItem('sessionType')=='worker'){
              // worker calls
        if(localStorage.getItem('sessionType')==='Client'){return false}
        fetch('http://localhost:3000/ordersWorker/getOngoingOrders/'+localStorage.getItem('sessionID'),{credentials:'include'})
        .then(res=>res.json())
        .then(res=>{
            try{
                console.log(res.result[0])
                this.setState({
                    ongoingJobs:res.result[0]
                })
            }
            catch(err){
                console.log(err)
            }
        })
        fetch('http://localhost:3000/dataservices/getallskills',{
            method:'GET',
            credentials:'include',
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res)
            try{
                res.recordset.forEach(record => {
                   
                    this.setState({
                        skills:this.state.skills.concat({value:record.SkillId,label:record.SkillTitle.charAt(0).toUpperCase()+record.SkillTitle.slice(1)})
                     
                    })
                    
                });
                console.log('SKILLS',this.state)
            }
            catch(err){
                console.log(err)
            }
        })
        .catch(err=>{
            console.log(err)
        })
        }
        var endpoint = 'http://localhost:3000/'+localStorage.getItem('sessionType').toLowerCase()+'/profile/'
     fetch(endpoint+localStorage.getItem('sessionID'),{credentials:'include'})
     .then(res=>res.json())
     .then(res=>{
        try{
            var usr1 = res.result.recordsets[0][0]
               
            const user ={
                firstName:usr1.FirstName,
                lastName:usr1.LastName,
                contactNumber:usr1.ContactNumber,
                profilePicture:'/avata.jpg'/*usr1.ImgUrl*/,
                BaseLocation:usr1.BaseLocation,
                skillSet:res.result.recordsets[1],
                rating:usr1.Rate,
                status:usr1.Status

            }
            this.setState({
                user:user
            })
        }
        catch(err){
            console.log(err)
        }
    })


}


    editable(){
        this.setState(state=>({
            editable:!state.editable
        }))
    }

    BLoc(e){

        this.setState({
            newLocation:e.value
        })
    }

    newSkill(e){
        alert(e.value)
        this.setState({
            newSkill:e.value
        })
    }
    onChange(k,v){
        this.setState({
           ['new'+k]:v
        },()=>console.log(this.state))

    }

    onSubmit(){
        var endpoint = 'http://localhost:3000/'+localStorage.getItem('sessionType').toLowerCase()+'/profile/'
        var payload = {
            fname:this.state.newfirstName?this.state.newfirstName:this.state.user.firstName,
            lname:this.state.newlastName?this.state.newlastName:this.state.user.lastName,
            baseL:this.state.newLocation?this.state.newLocation:this.state.user.BaseLocation,
            contactno:this.state.newcontactNumber?this.state.newcontactNumber:this.state.user.contactNumber
        }
        fetch(endpoint+localStorage.getItem('sessionID'),{
            credentials:'include',
            method:'PUT',
            body:JSON.stringify(payload),
            headers:{'content-type': 'application/json'}})
        .then(res=>res.json())
        .then(res=>{
            console.log(res)
            document.location.reload()
        })
    }

    updateSkills(){
        this.setState({
            newSkill:true
        })
    }
    closeSkills(){
        this.setState({
            newSkill:false
        })
    }

    deleteSkill(SkillId){

        fetch('http://localhost:3000/worker/skill/'+localStorage.getItem('sessionID'),{
            credentials:'include',
            method:'DELETE',
            headers:{'content-type': 'application/json'},
            body: JSON.stringify( { "skillId": parseInt(SkillId) })
        })
        .then(res=>res.json())
        .then(res=>{
            document.location.reload()
        })
    }

    addNewSkill(){
        const newSkill = {
            skillObj: [{
                skillId: this.state.newSkill,
                hrate: parseFloat(this.state.newHourlyCharge),
            }]
        }
        fetch('http://localhost:3000/worker/skill/'+localStorage.getItem('sessionID'),{
            credentials:'include',
            method:'POST',
            headers:{'content-type': 'application/json'},
            body: JSON.stringify( newSkill)
        })
        .then(()=>{
            document.location.reload()
        })
    }

    render(){
        if(this.state.user){
            return(
                <Row style={{paddingTop:'10px'}}>
                    {/* New Skill */}
                    <Modal show={this.state.newSkill}>
                    <Modal.Header>Add New Skill</Modal.Header>
                        <Modal.Body>
                            <Editor data = {{skill:null,HourlyCharge:null}}
                            onChange={this.onChange.bind(this)}
                                skill={{custom:<DropDown callback={this.newSkill.bind(this)} data={this.state.skills}/>}}
                                />
                      
                        </Modal.Body>
                        <Modal.Footer>
                        <Button onClick = {()=>this.addNewSkill()}>Add</Button>
                            <Button variant="secondary" onClick = {()=>this.closeSkills()}>Cancel</Button>

                        </Modal.Footer>
                    </Modal>

                    {/* Editor */}
                    <Modal show={this.state.editable} onHide={()=>this.editable()}>
                        <Modal.Header>Update Profile</Modal.Header>
                        <Modal.Body>
                            <Editor
                                data={this.state.user}
                                onChange={this.onChange.bind(this)}
                                profilePicture={{render:false}}
                                skillSet={{render:false}}
                                rating={{render:false}}
                                status={{render:false}}
                                BaseLocation={{custom:<DropDown callback={this.BLoc.bind(this)} data={this.state.locations}/>}}
                                />
                        </Modal.Body>
                        <Modal.Footer>
                        <Button variant="success" onClick={()=>this.onSubmit()}>
                            Save
                        </Button>
                        <Button variant="secondary" onClick={()=>this.editable()}>
                            Close
                        </Button>
                        </Modal.Footer>
                   
                    </Modal>
                    
                    <Col md={6} style={{marginBottom:'5px'}}>
                    <Card style={{boxShadow: '0px 4px 14px 0px rgba(0,0,0,0.09)'}}>
                    <Card.Header style={{color:'white',backgroundColor:'#343a40',padding:'1px',paddingLeft:'4px',verticalAlign:'0px'}}>General </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md={6}>
                            <Card.Title>{this.state.user.firstName?this.state.user.firstName + ' '+this.state.user.lastName :'Update Your Information'  }</Card.Title>
                            <h6>{this.state.user.BaseLocation}</h6>
                            {/* <GetLocationName location={this.state.user.BaseLocation?this.state.user.BaseLocation.split(','):'0,0'}/> */}
                            {localStorage.getItem('sessionType')==='worker'&&
                            <StarRatings

                            numberOfStars={5}
                            rating={this.state.user.rating?this.state.user.rating:0}
                            starDimension="20px"
                            />}
                            </Col>

                            <Col md={6}>
                                <Image width="30%" style={{float:'right'}} src={this.state.user.profilePicture} thumbnail></Image>
                            </Col>

                        </Row>
                    </Card.Body>
                    <Card.Footer>
                                <Button onClick={()=>this.editable()} style={{margin:'1px',paddingBottom:'0px',paddingTop:'0px',float:'right'}} variant="outline-secondary"><small>✎ Update</small></Button>
                            </Card.Footer>
                    </Card>
                    </Col>
                    {localStorage.getItem('sessionType')==='worker'&&<>
                
                    <Col md={12}>
                        <Card>
                            {/* Worker Only */}
                            <Card.Header style={{color:'white',backgroundColor:'#343a40',padding:'2px',paddingLeft:'4px'}}>Worker Options</Card.Header>
                            <Card.Body>
                                <Row>

                                
                            <Col md={6}>
                            <label>
                                <h5>I am available </h5>
                                <Switch   checked={this.state.user.status} />
                            </label><br></br>
                            
                                <h5>Ongoing Job(s)</h5>
                            <div style={{height:'200px',overflowY:'scroll'}}>
                                <ListGroup>

                                {this.state.ongoingJobs?this.state.ongoingJobs.map(job=>{
                                    return <ListGroup.Item >{job.FirstName?job.FirstName +' '+ job.LastName+' - '+job.OrderDate+' ':'n/a - '+job.OrderDate+' '}<a href={'job/'+job.OrderId}><small>View Job</small></a></ListGroup.Item>

                                }):<Loading/>}
                                </ListGroup>
                            </div>

                            </Col>
                            <Col md={6} style={{borderLeft:'0px solid black'}}>
                                {this.state.user.skillSet.length>0?
                                <Table hover style={{border:'1px solid black'}}>
                                <thead style={{color:'white',backgroundColor:'#342a40',borderBlack:'black'}}>
                                    <td>Skill</td>
                                    <td>Rate</td>
                                    <td></td>
                                </thead>
                                <tbody>

                                {this.state.user.skillSet.map((skill,k)=>{
                                    console.log(skill)
                                    return <tr>
                                        <td key={k}>{skill.SkillTitle/*Decode SkillCode*/}</td>
                                        <td key={k}>{skill.HourlyCharge+' LKR/hr'}</td>
                                        <td key={k}><Button variant='outline-danger' onClick={()=>this.deleteSkill(skill.SkillId)}>x </Button></td>
                                        </tr>
                                })}
                                </tbody>
                             
                                </Table>:<h5>Show Us What You've Got!</h5>}
                           
                                <div>
                                    <ButtonGroup>
                                       <Button onClick={()=>this.updateSkills()}>Add Skills</Button>
                                    </ButtonGroup>
                                </div>
                        </Col></Row>
                            </Card.Body>
                            <Card.Footer>

                            </Card.Footer>
                        </Card>
                    </Col></>}
                </Row>
            )
        }
        else{
            return(
                <Row style={{paddingTop:'10px'}}>
                <Col   md={12}>
                <Loading/>
                
                </Col>
            </Row>
                )
        }
    }
}