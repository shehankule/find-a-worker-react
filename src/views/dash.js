import React from 'react'
import {Container,Row,Col,ListGroup,Image} from 'react-bootstrap'
import Sidebar from 'react-sidebar';
import SideNavBar from '../components/Widgets/Navigation/sideNav'
import { BrowserRouter as Router, Route, Link,Switch } from "react-router-dom";
import SearchContainer from '../components/Request/SearchContainer';
import MyRequests from '../components/Request/MyRequests';
import RequestContainer from '../components/Request/RequestContainer'
import MyOrders from '../components/Request/MyOrders';
import  OrderContainer from '../components/Request/OrderContainer'
import UserProfile from '../components/User/profile';
import Loading from '../components/Widgets/Loading';

const mql = window.matchMedia(`(min-width: 800px)`);
export default class DashView extends React.Component{
    constructor(props){
        super(props)
        this.state={
            sidebarDocked: mql.matches,
            sidebarOpen: false
        }
        this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    componentWillMount() {
        if(localStorage.getItem('sessionType')==='worker' && document.location.pathname =='/client'){
            document.location.replace('/client/requests')
            return false
        }
        mql.addListener(this.mediaQueryChanged);
      }
     
      componentWillUnmount() {
        this.state.mql.removeListener(this.mediaQueryChanged);
      }
     
      onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
      }
     
      mediaQueryChanged() {
        this.setState({ sidebarDocked: mql.matches, sidebarOpen: false });
      }

    render(){
        return(
            <Container fluid={true} style={{margin:'0',width:'100%'}}>

                <Row style={{marginTop:'56px',padding:'0'}}>
                    <Router>
                       
                        <Sidebar
                        sidebar={<SideNavBar/>}
                        
                        open={this.state.sidebarOpen}
                        docked={this.state.sidebarDocked}
                        onSetOpen={this.onSetSidebarOpen}
                        
                        transitions={true}
                        styles={{root:{top:'56px'},content:{backgroundColor:'Azure'}, sidebar: {overflow:'hidden',width:'350px',background: "white" }}}
                        >
                
                        {/* CONTENT */}
                        <Col md={12} >
                 

                            <Switch>
                                {localStorage.getItem('sessionType')==='worker'?<Route path={"/worker"} exact component={MyRequests}/>:null}
                                <Route path={"/client/"} exact render={(props)=><SearchContainer {...props} isDash={true}/>} />
                                <Route path={"/client/requests"} exact component={MyRequests}/>
                                <Route path={"/client/view/:id"} exact component={RequestContainer}/>
                                <Route path={"/client/job/:id"} exact component={OrderContainer}/>
                                <Route path={"/client/jobs"} exact component={MyOrders}/>
                                <Route path ={"/client/profile"} exact render={(props)=> <UserProfile {...props}/>}/>
                                <Route path ={"/client/past"} exact render={(props)=> <><h5>Feature Not Available at the Moment</h5><Loading style={{marginTop:'100px'}}{...props}/></>}/>
                            </Switch>
                        </Col>

                        </Sidebar>  
                    </Router>     )
                </Row>
           </Container>
        )
    }
}