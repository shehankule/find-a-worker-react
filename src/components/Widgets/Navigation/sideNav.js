import React from 'react';
import {Button, ListGroup,Row,Col} from 'react-bootstrap'
import UserApplet from '../../User/UserApplet'
import {Link} from 'react-router-dom'
export default class SideNavBar extends React.Component{
    constructor(props){
        super(props)
        this.path = document.location.pathname.split('/')
        this.pathlast = this.path[this.path.length-1]
    
        this.state={
            active:this.pathlast
        }
    }
    logout(){
        fetch('http://localhost:3000/user/logout',{
            method:'POST',
            credentials:'include'
        })
        .then((res)=>{
            console.log(res)
            localStorage.removeItem('sessionEmail')
            localStorage.removeItem('sessionType')
            document.location.reload()
        })
    }
    setActive(state){
        this.setState({
            active:state
        })

    }
    
    render(){
        
        return (
            <ListGroup variant="flush" style={{padding:'0px'}}>


                <ListGroup.Item >
                    <UserApplet dash={true}/>
                </ListGroup.Item >
                {/* Set Active from state */}

                <ListGroup.Item as="li" active={this.state.active==='profile'?true:false}>
                <Link onClick={()=>this.setActive('profile')} style={{color:'Black'}} to='/client/profile' >My Profile</Link>
                </ListGroup.Item>
                
              {localStorage.getItem('sessionType')==='Client'&&
                  <ListGroup.Item as="li" active={this.state.active==='client'?true:false}>
                  <Link onClick={()=>this.setActive('client')} style={{color:'Black'}} to='/client' >Find a Worker</Link>
                </ListGroup.Item>
              }  
                <ListGroup.Item as="li"  active={this.state.active==='requests'?true:false}>
                <Link onClick={()=>this.setActive('requests')} style={{color:'Black'}} to='/client/requests'  >{localStorage.getItem('sessionType')==='Client'?'Pending Requests':'Matching Jobs'}</Link>
                    
                </ListGroup.Item>
                <ListGroup.Item  active={this.state.active==='jobs'?true:false}>
                <Link onClick={()=>this.setActive('jobs')} style={{color:'Black'}} to='/client/jobs'>{localStorage.getItem('sessionType')==='worker'?'Upcoming Jobs':'My Jobs'}</Link>
                </ListGroup.Item>
                
                <ListGroup.Item as="li" active={this.state.active==='past'?true:false}>
                <Link onClick={()=>this.setActive('past')} style={{color:'Black'}} to='/client/past' >Past Jobs</Link>
                </ListGroup.Item>

                {/* {localStorage.getItem('sessionType')==='worker'&&
                  <ListGroup.Item as="li" active={this.state.active==='earnings'?true:false}>
                  <Link  onClick={()=>this.setActive('earnings')} style={{color:'Black'}} to='/client/earnings' >My Earnings</Link>
                </ListGroup.Item>
              }   */}
              
                <Row>
                
                <Col style={{paddingTop:'10px'}}md={{ span: 3, offset: 8 }}>
                <Button onClick={this.logout.bind(this)}variant="outline-dark">Logout</Button>
                </Col>
                </Row>
            </ListGroup>
        )
    }
} 