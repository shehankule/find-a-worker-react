import React from 'react'
import MapViewer from './MapViewer'

export default class LocationPicker extends React.Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    componentDidMount(){
        this.getCurrentLocation()
    }

    getCurrentLocation(pos){
        if(navigator.geolocation) {
            return navigator.geolocation.getCurrentPosition((pos)=>{
                console.log(pos.coords)
                this.setState({
                    lat:pos.coords.latitude,
                    lng:pos.coords.longitude
                })
            });
        }
        else{
            this.setState({
                lat:'7.8731',
                lng:'80.7718'
            })
        }
    }

    clickCB(e,map,coords){
        this.props.callback(coords)
    }

    dragCB(props,map){
        console.log(props.center)
         this.setState({
            lat:map.center.lat(),
            lng:map.center.lng()
        })
       
    }

    render(){
        return(
          
            <MapViewer
                center = {{lat:this.state.lat,lng:this.state.lng}}
                marker={this.props.marker?this.props.marker:null}
                style={this.props.styles?this.props.styles:null}
                callback = {this.clickCB.bind(this)}
                callback2 = {this.dragCB.bind(this)}
                population= {this.props.population?this.props.population:null}
                />
             
        
                
       
        )
    }
} 