import React from 'react'
import { Spinner,Alert } from 'react-bootstrap';
export default function GetLocation(latlng,callback,id=0){
    // console.log("REQUESTING:"+latlng)
    
    fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng[0]+','+latlng[1]+'&key=AIzaSyAHBtICwLwAX33NodlVFVUVgdO_xdK9ViA')
   .then(res=>res.json())
   .then(res=>{
       try{
           callback('location',res.results[0].formatted_address,id=0)
         
       }
       catch(err){
           callback('location',<Alert variant="danger" style={{fontSize:'12px',padding:'5px',display:'inline'}} >
               N/A : Location Not Found
         </Alert>,id=0)
        //    console.log(err)
       }
       })
       
}

export class GetLocationName extends React.Component{
    constructor(props){
        super(props)
        this.state={
            location:<div><Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner> <small>Please Wait</small></div>
        }
    }

    componentDidMount(){
    if(this.props.location){
        console.log(this.props.location)
        GetLocation(this.props.location,(key,value)=>{
            this.setState({
                [key]:value
            })
        })
    }
    }
    
    render(){
        if(this.state.location===null)
            {return null}
    return(
        <p>{this.state.location}</p>
        )
    }
}