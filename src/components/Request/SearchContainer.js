import React from 'react';
import LocationPicker from '../Widgets/LocationPicker';
import {Accordion,Row,Col} from 'react-bootstrap'
import Searchbar from '../Widgets/Searchbar';
import DateTimePicker from '../Widgets/DateTimePicker'
import formatDate from '../../helpers/formatDate'
import LoadingOverlay from 'react-loading-overlay';

export default class SearchContainer extends React.Component{
    constructor(props){
        super(props);
        this.date = new Date(Date.now())
        console.log(this.date.toTimeString())
        console.log()
        this.state={
            skills:[],
            lat:'7.8731',
            lng:'80.7718',
            OrderLocationNew:{},
            StartTime:this.date.toTimeString().split(' ')[0],
            ExpectedEndTime:this.date.toTimeString().split(' ')[0],
            OrderDate: formatDate(this.date)
        }
    }
    componentWillMount(){
       if(navigator.geolocation){
           navigator.geolocation.getCurrentPosition((pos)=>{
            var lat= pos.coords.latitude
            var lng = pos.coords.longitude
               this.setState({
                    OrderLocationNew:{lat,lng} 
               })
           })
       }
        fetch('http://localhost:3000/dataservices/getallskills',{
            method:'GET',
            credentials:'include',
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res)
            res.recordset.forEach(record => {
               
                this.setState({
                    skills:this.state.skills.concat({value:record.SkillId,label:record.SkillTitle.charAt(0).toUpperCase()+record.SkillTitle.slice(1)})
                 
                })
                
            });
            console.log(this.state)
        })
        .catch(err=>{
            console.log(err)
        })


    }

   
    setOrderLocation(coords){
        const lat = coords.latLng.lat()
        const lng = coords.latLng.lng()
        this.setState({
            OrderLocationNew:{lat,lng} 
        },()=>{this.getWorkers()})
       
    }

    handleChangeCB(k,v){
        this.setState({[k]:v},()=>{
            if(k=="SkillId" ){
                this.getWorkers()
            }
            else{
                this.getWorkersLater()
            }
        })
    }

    handleSubmitCB(){
        if(this.state.population===undefined){
           this.setState({
                loading:true
            })
            setTimeout(()=>{this.setState({
                loading:false
            })
        },1000)
        return false
        }

        let workerids=[]
        this.state.population.workers.forEach(worker=>{
            workerids.push(worker.WorkerId)
        })
        if(workerids.length===0){
            alert('NO WORKERS FOUND ALERT')
            return false
        }
        console.log(workerids)
   
        var payload = {
        clientId:localStorage.getItem('sessionID'),
        jobTypeId:this.state.SkillId,
        orderDate:this.state.OrderDate,
        location:this.state.OrderLocationNew.lat+','+this.state.OrderLocationNew.lng,
        workers:workerids
        }

        fetch('http://localhost:3000/booknow/sendUrgentRequest',{
            headers:{'content-type': 'application/json'},
            method:'POST',
            credentials:'include',
            body:JSON.stringify(payload)
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res)
            document.location.replace('client/requests')
        })
        // console.log(payload)
    }

    handleSubmitLaterCB(){
        alert('SUBMITTING ALERT')
    }
    
    getWorkers(){
        var payload = {
        clientId:localStorage.getItem('sessionID'),
        jobType:this.state.SkillId,
        cordinate:this.state.OrderLocationNew.lat+','+this.state.OrderLocationNew.lng,
        Workers:[]
        }

        fetch('http://localhost:3000/booknow/booknow',{
            headers:{'content-type': 'application/json'},
            method:'POST',
            credentials:'include',
            body:JSON.stringify(payload)
        })
        .then(res=>{
            this.setState({
                loading:true
            })
            return res.json()
        })
        .then(res=>{
        setTimeout(()=>{
            this.setState({
                population:res.result,
                loading:false,
            },()=>console.log(this.state))
        },1000)
        })
    }

    getWorkersLater(){
        var payload = {
            skillId: this.state.SkillId,
            orderDate: this.state.OrderDate,
            startTime: this.state.StartTime,
            endTime: this.state.ExpectedEndTime,
            clientId: localStorage.getItem('sessionID'),
            coordinates:this.state.OrderLocationNew.lat+','+this.state.OrderLocationNew.lng,
        }
       

        fetch('http://localhost:3000/bookLater/search',{
            headers:{'content-type': 'application/json'},
            method:'POST',
            credentials:'include',
            body:JSON.stringify(payload)
        })
        .then(res=>{
            this.setState({
                loading:true
            })
            return res.json()
        })
        .then(res=>{
            console.log('BOOK_LATER_RESULTS',res)
        setTimeout(()=>{
            this.setState({
                // population:res.result,
                loading:false,
            })
        },1000)
        })
    }

    render(){
        return (
            <>  
            <Accordion>
                 <Row style={{margin:'0'}}>
                     {!this.props.isDash&&<Col md={2} sm={2} xs={12}></Col>}
                     <Col md={this.props.isDash?6:8} sm={8} xs={12}>
                    <LoadingOverlay  active={this.state.loading} spinner text={this.state.SkillId?'Finding Professionals Around You...':"Please Select A Job Type..."}>
                    {/* {this.state.loading?<Loading style={{top:'0',display:'absolute'}} />:null} */}

                     <LocationPicker
                        marker={this.state.OrderLocationNew} 
                        callback = {this.setOrderLocation.bind(this)}
                        population = {this.state.population}
                        styles={!this.props.isDash?{
                  
                            boxShadow: '0px 9px 14px 0px rgba(0,0,0,0.29)'}:
                            {
                           
                                marginTop:'20px',
                                height:'800px',
                                boxShadow: '0px 9px 14px 0px rgba(0,0,0,0.29)'}}
                            
                            />
                   {!this.props.isDash&& <Searchbar bookNow={this.handleSubmitCB.bind(this)} callback={this.handleChangeCB.bind(this)} dropdown={this.state.skills} /> }
                    {this.props.isDash&& <div style={{height:'820px',top:'200px'}}></div>}
                    </LoadingOverlay>
                    </Col>
                    {!this.props.isDash&&<Col md={2} sm={2} xs={12}></Col>}
                    {this.props.isDash&&<Col md={6}>
                    <Searchbar bookNow={this.handleSubmitCB.bind(this)} callback={this.handleChangeCB.bind(this)} dropdown={this.state.skills}  sides={'none'}top={'20px'}/> 
                    <Accordion.Collapse eventKey="0">
                    <DateTimePicker bookLater={this.handleSubmitLaterCB.bind(this)} callback={this.handleChangeCB.bind(this)} StartTime={this.state.StartTime} EndTime={this.state.ExpectedEndTime} OrderDate={this.state.OrderDate}/>
                    </Accordion.Collapse>
                </Col>}
                </Row>
                   {!this.props.isDash&&<Row  style={{padding:'0',margin:'0'}}>
                        <Col md={2} sm={2}></Col>
                        <Col md={8} sm={8} xs={12} >
                        <Accordion.Collapse eventKey="0">
                            <DateTimePicker bookLater={this.handleSubmitLaterCB.bind(this)}  callback={this.handleChangeCB.bind(this)} StartTime={this.state.StartTime} EndTime={this.state.ExpectedEndTime} OrderDate={this.state.OrderDate}/>
                        </Accordion.Collapse>
                        </Col>
                    <Col md={2} sm={2} xs={12}></Col>
                </Row> }             
             
                            </Accordion>
            </>
        )
    }
}