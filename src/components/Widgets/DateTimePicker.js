import React from 'react'
import {Form,Col,Row,Button} from 'react-bootstrap'

export default class DateTimePicker extends React.Component{
    onChange(e){
        this.props.callback(e.target.name ,e.target.value)
    }

    render(){
        return(
            <Form>
                <Row style={{margin:'0',paddingTop:'10px',marginRight:'0',backgroundColor:'rgba(100, 100, 100, 0.2)'}}>
             
                    <Col md={4} sm={12}>
                    <Form.Group style={{position:'',display:'inline'}} controlId="RequestDate">
                        <Form.Control onChange={this.onChange.bind(this)} name="OrderDate" value={this.props.OrderDate} type="date"/>
                        <Form.Text>Pick a Date</Form.Text>
                    </Form.Group>
                    </Col>
                    <Col md={4} sm={12}>
                    <Form.Group style={{position:'',display:'inline'}}>
                        <Form.Control onChange={this.onChange.bind(this)} name="StartTime" value={this.props.StartTime} type="time" />
                        <Form.Text>At time what should work begin?</Form.Text>
                    </Form.Group>
                    </Col>
                    <Col md={4} sm={12}>
                    <Form.Group style={{position:'',display:'inline'}}>
                        
                        <Form.Control onChange={this.onChange.bind(this)} name="ExpectedEndTime" value={this.props.EndTime}  type="time" />
                        <Form.Text>At what time should the work be done?</Form.Text>
                    </Form.Group>
                    </Col>
                    <Col md={12}>
                        <Button onClick={this.props.bookLater} style={{float:'right',margin:'5px'}}>Confirm</Button>
                    </Col>
                </Row>
                
            </Form>
            
        )
    }
}