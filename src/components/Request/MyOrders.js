import React from 'react'
import {Table,} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import getLocation from '../../helpers/getLocation'
import GetSkillName from '../../helpers/getSkillName';
import GetLocation ,{GetLocationName} from '../../helpers/getLocation';
import Loading from '../Widgets/Loading';

export default class MyOrders extends React.Component{
    constructor(props){
        super(props)
        this.state={
            data:null,
            location:null
        }
    }

    componentWillMount(){
        if(localStorage.getItem('sessionType')==='worker'){
            fetch('http://localhost:3000/ordersWorker/getUpComingOrders/'+localStorage.getItem('sessionID'),{credentials:'include'})
            .then(res=>res.json())
            .then(res=>{
                console.log(res)
                if(res.result!==undefined){
                    this.setState({
                        data:res.result[0]
                    })
                }else{
                    this.setState({
                        data: false
                    })
                }
            })
        }
        else{
            
            fetch('http://localhost:3000/ordersClient/upcomingjobs/'+localStorage.getItem('sessionID'),{credentials:'include'})
            .then(res=>res.json())
            .then(res=>{
                console.log(res)
                if(res.result!==undefined){
                    this.setState({
                        data:res.result[0]
                    })
                }else{
                    this.setState({
                        data: false
                    })
                }
            })
        }
    }
    render(){
        const data = this.state.data
 
        if (data===null){return <Loading/>}
        if(data===false){return <p>No Data Found</p>}
        return(
            <Table responsive hover>
                
                    <thead>
                        <th>#</th>
                        <th>{localStorage.getItem('sessionType')==='worker'?'Name':'Worker Name'}</th>
                        <th>Location</th>
                        <th>Contact</th>
                        <th>Order Date</th>
                        <th>Expected Start Time</th>
                        <th>Expected End Time</th>
                        <th></th>
                    </thead>

                    <tbody>
                    {data?data.map((request,key)=>{
                       
                                console.log(request)
                              return <tr>
                                  <td>{key+1}</td>
                                  <td>{request.FirstName?request.FirstName.charAt(0).toUpperCase() +request.FirstName.slice(1) +' '+request.LastName.charAt(0).toUpperCase() +request.LastName.slice(1):null}</td>
                                  
                    <td>{localStorage.getItem('sessionType')==='worker'?<GetLocationName location={request.OrderLoaction!==undefined?request.OrderLoaction.split(','):null}/>:request.BaseLocation}</td>
                                  <td>{request.ContactNumber}</td>
                                  <td>{ request.OrderDate }</td>
                                  <td>{request.ExpectedStartTime}</td>
                                  <td>{request.ExpectedEndTime}</td>
                                  <td><Link to={"/client/job/"+request.OrderId}>View</Link></td>
                              </tr>
                          }):null}
                    </tbody>
            </Table>
        )
    }
}