import React from 'react'
import {Container,Row,Col,Image} from 'react-bootstrap'
import SearchContainer from '../components/Request/SearchContainer'

export default class HomeView extends React.Component{
    render(){
        return(
            <Container fluid={true} style={{margin:'0',width:'100%'}}>
                
                <Row>
                <Col md={12} sm={12} xs={12} style={{padding:'0',margin:'0'}}>
                    <SearchContainer />
                </Col>
                </Row>

                <Row>
                    <Col md={2} sm={2} xs={12}></Col>
                     <Col md={8} sm={8} xs={12}>
                         <section id="about">

                        <h1>On the Lookout For Professional Help?</h1>
                            <Row>
                            <Col md={4}>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </Col>  
                            <Col md={8}>
                            <Image width="60%"src='./undraw_searching_p5ux.svg'/>
                            </Col>
                            </Row>
                         </section>
                        </Col>
                    <Col md={2} sm={2} xs={12}></Col>

                </Row>
                
            </Container>
        )
    }
}