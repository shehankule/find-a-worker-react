import React from 'react'
import { Container,Form,Button,Row,Col,Image,Alert } from 'react-bootstrap';

export default class UserLogin extends React.Component{
    constructor(props){
        super(props)
        this.state={}
    }
    handleSubmit(e){
        e.preventDefault()
        const payload = {
            UserEmail:e.target[0].value,
            Password:e.target[1].value
        }

      
        
        fetch('http://localhost:3000/user/login',{
            'method':'POST',
            'headers': {
                "Content-Type": "application/json"
              },
            'credentials':'include',
            'body':JSON.stringify(payload)
            // body:payload
        }).then(res=>res.json())
        .then(res=>{
            if(res.status===200){
                console.log(res.result)
                localStorage.setItem('sessionEmail',res.result.sessionEmail)
                localStorage.setItem('sessionType',res.result.sessionType)
                localStorage.setItem('sessionID',res.result.UserId)
                document.location.pathname!=='/login'?document.location.reload():document.location.replace('/client/profile')
                                
            }
            else{
                this.setState({
                    error:true
                })
            }
        })

    }
        onclick(e){
            fetch('http://localhost:3000/requests/show',{
                'credentials':'include'
            })
            .then(res2=>res2.json())
            .then(res2=>console.log(res2))
        }
        onclick2(e){
            fetch('http://localhost:3000/user/logout',{
                'method':'POST',
                'credentials':'include'
            })
            .then()
            // .then(res2console.log(res2))
        }

    render(){
        return(
            
                <Container fluid={true} style={{margin:'0'}}>

                  <Row styles={{margin:'0',padding:'0'}}>
                <Col md={{span:4,offset:4}}>
                <Image src="/undraw_authentication_fsn5.svg" width={"100%"}></Image >
                </Col>
                </Row>
                <Row>
                <Col md={{span:4,offset:4}}>
                <h3>Say the Magic Word!</h3>
                {this.state.error  && <Alert id="Alert" onClose={()=>{this.setState({error:false})}} variant="danger"  dismissible>Invalid Login</Alert>}
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <Form.Group>
                        <Form.Text>Email</Form.Text>
                        <Form.Control name ="UserEmail" type="text"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Text>Password</Form.Text>
                        <Form.Control type="password"></Form.Control>
                    </Form.Group>
                    <Button  type="submit">Login</Button>
                    <Button variant="link" onClick={()=>alert('Feature Not Available')}>Forgot Password?</Button>
                </Form>
                </Col>
                
                </Row>
                </Container>
              
    
            
        )
    }
}