import React from 'react'
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

const MapContainer = function(props){
    
    return(
        <>
        
        <Map
            google={props.google?props.google:15}
            minZoom={8}
            zoom={props.zoom}
            style={props.style?props.style:null}
            initialCenter={props.center}
            center={props.center}
            onClick ={props.callback?props.callback:null}
            disableDefaultUI={true}
            onDragend={props.callback2?props.callback2:null}
            >
            {props.marker?<Marker 
                position={props.marker} 
                animation={props.google.maps.Animation.DROP}
                />:null}
                {props.population?populate(props):null}
        
  
        
        </Map>

                </>
        );

}

function populate(props){
 
    var image = {
        url: "/marker.gif", // url
        scaledSize: new props.google.maps.Size(75, 75), // scaled size
        origin: new props.google.maps.Point(0,0), // origin
        anchor: new props.google.maps.Point(0, 0) // anchor
    };

    return props.population.workers.map(marker=>{
        let coords = {lat: marker.Latitude, lng:marker.Longitude}
        return <Marker 
        position={coords} 
        icon= {image}
        symbol={{fillColor:'white'}}
 
        onClick={()=>alert(marker.FirstName)}
        
        />
    })
}



export default GoogleApiWrapper({
    apiKey: 'AIzaSyAHBtICwLwAX33NodlVFVUVgdO_xdK9ViA'
  })(MapContainer);