import React from 'react'
import {Button} from 'react-bootstrap'
import { async } from 'q';


export default function(props){

    var isWorker  = props.isWorker //set with session isWorker tag
  console.log(props)
    if(!isWorker){
        return null
    }
    else{
        if(localStorage.getItem('sessionType')!=='worker'){return null}
        return(
            <div className = "">
             <br/>
                {/* {props.isStarted===null&&<Button >Reject Job</Button>} */}
                {props.isStarted===null&&<Button value={props.id} onClick={(e)=>startJob(props.Id)} style={{margin:'5px'}}>Start Now</Button>}
                {props.isStarted!==null && props.isEnded===null?<Button onClick={()=>endJob(props.isStarted,props.Id)} style={{margin:'5px'}} >Complete Task</Button>:null}
            </div>
        )
    }
}


function startJob(id){
    var time = new Date()
    time = time.toTimeString().split(' ')[0]
    var payload = {
        OrderId:id,
        StartTime: time
    } 

    fetch('http://localhost:3000/ordersWorker/startOrder',{
        headers:{'content-type': 'application/json'},
        credentials:'include',
        method:'PUT',
        body:JSON.stringify(payload)
    })
    .then(res=>res.json())
    .then(res=>{
        document.location.reload()
    })
}

function endJob(start,id){
    var time = new Date()
    var starttime = new Date(start)

    time = time.toTimeString().split(' ')[0]
    var payload = {
        OrderId:id,
        StartTime:starttime.toUTCString().split(' ')[4],
        EndTime: time
    } 

    fetch('http://localhost:3000/ordersWorker/endOrder',{
        headers:{'content-type': 'application/json'},
        credentials:'include',
        method:'PUT',
        body:JSON.stringify(payload)
    })
    .then(res=>res.json())
    .then(res=>{
        document.location.replace('/client/profile')
    })
}
