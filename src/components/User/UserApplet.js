import React from 'react'
import {Col,Image,Nav} from 'react-bootstrap'


export default class UserApplet extends React.Component{
    constructor(props){
        super(props)
        this.state={
            data:null,
            
        }
    }

    componentDidMount(){
        if(localStorage.getItem('sessionType')==='worker'){
            fetch('http://localhost:3000/worker/profile/'+localStorage.getItem('sessionID'),{credentials:'include'})
            .then(res=>res.json())
            .then(res=>{
                console.log(res.result)
                if(res.result!==undefined){
                    try{
                        this.setState({
                            data:true,
                            first:res.result.recordsets[0][0].FirstName?res.result.recordsets[0][0].FirstName:localStorage.getItem('sessionEmail'),
                            last:res.result.recordsets[0][0].LastName?res.result.recordsets[0][0].LastName:'',
                            base:res.result.recordsets[0][0].BaseLocation,
                            img:'http://'+res.result.recordsets[0][0].ImgUrl
                        })
                    }
                    catch(err){
                        console.log(err)
                    }
                }
            })
        }   
        else{
            fetch('http://localhost:3000/client/profile/'+localStorage.getItem('sessionID'),{credentials:'include'})
            .then(res=>res.json())
            .then(res=>{
                console.log(res.result)
                if(res.result!==undefined){
                    this.setState({
                        data:true,
                        first:res.result.recordsets[0][0].FirstName?res.result.recordsets[0][0].FirstName:localStorage.getItem('sessionEmail'),
                        last:res.result.recordsets[0][0].LastName?res.result.recordsets[0][0].LastName:'',
                        base:res.result.recordsets[0][0].BaseLocation,
                        img:'http://'+res.result.recordsets[0][0].ImgUrl
                    })
                }
            })
        }
    }

    render(){
        if(this.props.dash){
            if(!this.state.data){return null}
            return(
                <Col  xs={6} md={12} style={{color:'white'}}>
                {/* <Image style={{marginRight:'5px'}}src={this.state.img} width="50" roundedCircle /> */}
                   <div style={{color:'black'}}>

                   <h4 id="username" >
                        {this.state.first +' '+ this.state.last}
                       </h4>
                       {this.state.last===''?<a href="profile">Complete Details</a>:null}
                    <p>{this.state.base}</p>
                   </div>
              </Col>
            )
        }
        else{
            return (
                <div className="">
        
                    {/* Avatar Here */}
                    <img alt="" width="50"/>
        
                    {this.props.UserId  /*Get UserName from the id Passed*/} Sample Name<br/> 
                    <strong>Contact Number {/*Get User Contact Number*/}</strong> 
                </div>
            )
        }
    }
}