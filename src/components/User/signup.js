import React from 'react'
import { Container,Form,Button,Row,Col,Image } from 'react-bootstrap';

export default class SignUp extends React.Component{
    handleSubmit(e){
        e.preventDefault()
        if(e.target[1].value!==e.target[2].value){alert('PASSWORDS DO NOT MATCH'); return false}
        const payload = {
            UserEmail:e.target[0].value,
            Password:e.target[1].value,
            ContactNumber: e.target[3].value,
            UserType: e.target[4].checked
        }

      
 
        fetch('http://localhost:3000/user/register',{
            'method':'POST',
            'headers': {
                "Content-Type": "application/json"
              },
            'credentials':'include',
            'body':JSON.stringify(payload)
            // body:payload
        }).then(res=>res.json())
        .then(res=>{
            if(res.status===201){
                alert("USER CREATED")
                document.location.replace('/login')                  
            }
            else{
                
            }
        })

    }


    render(){
        return(
            
                <Container fluid={true} style={{margin:'0'}}>

                  <Row styles={{margin:'0',padding:'0'}}>
                <Col md={{span:4,offset:4}}>
                <Image src="/undraw_detailed_information_3sp6.svg" width={"100%"}></Image >
                </Col>
                </Row>
                <Row>
                <Col md={{span:4,offset:4}}>
                <h3>Join Us!</h3>
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <Form.Group>
                        <Form.Text>Email</Form.Text>
                        <Form.Control name ="UserEmail" type="text"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Text>Password</Form.Text>
                        <Form.Control type="password"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Text>Confirm Password</Form.Text>
                        <Form.Control type="password"></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Text>Contact Number</Form.Text>
                        <Form.Control type="text" pattern="[0-9]*" maxLength="10"></Form.Control>
                    </Form.Group>

                    <Form.Check type='checkbox'
        label={`I'm Signing Up For A Worker Account`}
      />

                    <Button style={{float:'right'}} variant="link" onClick={()=>alert('Feature Not Available')}>Forgot Password?</Button>
                    <Button style={{float:'right'}} type="submit">Sign Up</Button>
                </Form>
                </Col>
                
                </Row>
                </Container>
              
    
            
        )
    }
}