import React from 'react';
import LocationPicker from '../Widgets/LocationPicker';
import {Accordion,Row,Col} from 'react-bootstrap'
import Searchbar from '../Widgets/Searchbar';
import DateTimePicker from '../Widgets/DateTimePicker'

export default class SearchContainer extends React.Component{
    constructor(props){
        super(props);
        this.state={
            skills:[]
        }
    }
    componentDidMount(){
        console.log("loading")
        fetch('http://localhost:3000/dataservices/getallskills',{
            method:'GET',
            credentials:'include',
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res)
            res.recordset.forEach(record => {
               
                this.setState({
                    skills:this.state.skills.concat({value:record.SkillId,label:record.SkillTitle.charAt(0).toUpperCase()+record.SkillTitle.slice(1)})
                })
                
            });
            console.log(this.state)
        })
    }

    setOrderLocation(coords){
        const lat = coords.latLng.lat()
        const lng = coords.latLng.lng()
        this.setState({
            OrderLocationNew:{lat,lng} 
        },()=>(console.log(this.state.OrderLocationNew)))
       
    }

    render(){
        return (
            <>  
            <Accordion>

   
            <Row>

           
                   
                  <Col md={6}>
                     <LocationPicker
                        marker={this.state.OrderLocationNew} 
                        callback = {this.setOrderLocation.bind(this)}
                        center = {{ lat: 6.9128, lng: 79.8507}}
                        styles={{
                            right:'2.5%',
                            marginTop:'20px',
                            height:'800px',
                            boxShadow: '0px 9px 14px 0px rgba(0,0,0,0.29)'}}
                            />
                  </Col>
                  <Col md={6}>
                    <Searchbar dropdown={this.state.skills} sides={'none'}top={'20px'}/> 
                    <Accordion.Collapse eventKey="0">
                    <DateTimePicker/>
                    </Accordion.Collapse>
                </Col>
                   
                   
               
                </Row>         </Accordion>
            </>
        )
    }
}