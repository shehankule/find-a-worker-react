import React from 'react'
import {Navbar,Nav,Form,Container,FormControl} from 'react-bootstrap'

function logout(){
    fetch('http://localhost:3000/user/logout',{
        method:'POST',
        credentials:'include'
    })
    .then((res)=>{
        console.log(res)
        localStorage.removeItem('sessionEmail')
        localStorage.removeItem('sessionType')
        localStorage.removeItem('sessionID')
        document.location.replace('/')
    })
}
export default function TopNav(){

    return (
        <Container fluid={true} style={{margin:'0',padding:'0'}}>

        <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="/">Find-A-Worker</Navbar.Brand>
        <Nav className="mr-auto">
                <Nav.Link href="/">About</Nav.Link>
                
                <Nav.Link href="#services">Services</Nav.Link>
      
        </Nav>
        <Form inline>
        <Nav className="mr-auto">
        {!localStorage.getItem('sessionEmail')?<Nav.Link href="/signup">Sign Up</Nav.Link>:null}
        <Nav.Link href={!localStorage.getItem('sessionEmail')?"login":"/client"}>{!localStorage.getItem('sessionEmail')?'Login':localStorage.getItem('sessionEmail')}</Nav.Link>
        {!localStorage.getItem('sessionEmail')?null:<Nav.Link onClick={logout.bind(this)}>Logout</Nav.Link>}
        </Nav>
        </Form>
        </Navbar>
        </Container>
    )
}