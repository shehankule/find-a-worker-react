import React from 'react'
import  {Alert} from 'react-bootstrap'
export default function err(){
    return(
        <Alert variant='danger'> 
            <h4>Not Found!</h4>
        </Alert>
    )
}