import React from 'react'
import {Button} from 'react-bootstrap'
import { async } from 'q';


export default function(props){

    var isWorker  = props.isWorker //set with session isWorker tag
  console.log(props.id)
    if(!isWorker){
        return null
    }
    else{
        if(localStorage.getItem('sessionType')!=='worker'){return null}
        return(
            <div className = "">
             <br/>
                <Button value={props.id} onClick={(e)=>handleSubmit(props.Id)} style={{margin:'5px'}}>Accept</Button>
                <Button >Ignore</Button>
            </div>
        )
    }
}

 function handleSubmit(id){
    
     console.log(id)
     fetch('http://localhost:3000/requests/accept/'+id,{
        headers:{'content-type': 'application/json'},
         method:'POST',
         credentials:'include',
         body:JSON.stringify({WorkerId: localStorage.getItem('sessionID')})
        })

    .then(res=>{
        res.json()
        console.log(res)
    })
    .then(res=>{
        console.log(res)
        document.location.replace('/client/jobs')
    })
}